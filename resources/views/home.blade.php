@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1> Products: </h1>

                    @if(count($products))
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Body html (cut)</th>
                            <th scope="col">Price</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Updated at</th>

                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <th scope="row">{{ $product['id'] }}</th>
                                <td>{{ $product['title'] }}</td>
                                <td>{{ substr($product['body_html'], 0, 60 ) . '...' }}</td>
                                <td>
                                @if(count($product['variants']) == 1)
                                    {{ reset($product['variants'])['price'] }}
                                @else
                                    @foreach($product['variants'] as $k => $v)
                                        ID: {{ $k }} | Price {{ $v['price'] }}
                                    @endforeach
                                @endif
                                </td>
                                <td>{{ $product['created_at'] }}</td>
                                <td>{{ $product['updated_at'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    Products Pages: 
                    @for ($i=1; $i <= $paginationInfoForProducs['lastPage']; $i++)
                            
                            @if($i == $paginationInfoForProducs['actualPage'])
                                {{ $i }}
                            @else 
                                <a href="{{ route('home') }}?productPage={{ $i }}">{{ $i }}</a> 
                            @endif
    
                        @endfor

                    @else
                        No have products...
                    @endif

                    <br>
                    <br>

                    <h1> Orders: </h1>
                    <br>

                    @if(count($orders))
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Client Email</th>
                            <th scope="col">Total Price</th>
                            <th scope="col">Detail Order Link</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Updated at</th>

                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <th scope="row">{{ $order['id'] }}</th>
                                <th scope="row">{{ $order['email'] }}</th>
                                <th scope="row">{{ $order['total_price'] }}</th>
                                <th scope="row"><a href="{{ $order['order_status_url'] }}" target="_blank">Link to open</a></th>
                                <td>{{ $order['created_at'] }}</td>
                                <td>{{ $order['updated_at'] }}</td>
                            </tr>
                        @endforeach 
                        </tbody>
                    </table>

                   

                    Orders Pages: 
                    @for ($i=1; $i <= $paginationInfoForOrders['lastPage']; $i++)
                            
                        @if($i == $paginationInfoForOrders['actualPage'])
                            {{ $i }}
                        @else 
                            <a href="{{ route('home') }}?orderPage={{ $i }}">{{ $i }}</a>
                        @endif

                    @endfor

                    @else
                        No have orders...
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
