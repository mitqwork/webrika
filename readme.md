Задача

Изграждане на минималистичен администраторски панел със следните функционалности
Защитен достъп с Login
Добавяне на пакет за роли на потребителите
Менежиране на продукти от Shopify
Вземане на всички продукти от магазин (отдолу е достъпа до API-a на демо магазина за задачата)
Листване на продуктите 
Редактиране на продукт  (Само заглавие , цена и описание. Продуктите ще са без вариации/опции)
Менежиране на поръчки от Shopify
Вземане на всички поръчки от магазина 
Листване на поръчките от магазина
Детайлен изглед на поръчка
User management



Насоки и ресурси


Интеграция със Shopify - Посредством Shopify rest API - https://help.shopify.com/en/api/reference
Взимане на всички продукти от магазина - https://help.shopify.com/en/api/reference/products/product#index-2019-04
Листване на всички продукти
Редактиране на продукт - https://help.shopify.com/en/api/reference/products/product#update-2019-04
Взимане и показване на всички поръчки - https://help.shopify.com/en/api/reference/orders/order#index-2019-04
Листване на всички поръчки
Можеш да използваш този пакет за интеграцията - phpclassic/php-shopify

Бонус ще е ако се направят някой елементи на vuejs, но не е задължително.



Достъп до Shopify тестов магазин



Shop URL
venera-cosmetics2.myshopify.com
API key
c2e63a90f724d17c342fb66746c59d16
API password
751d06b48f01880132b85f2a3b463cd4
Shared secret
d9393d4cb5ef020c6fd562c52ced8ebb


