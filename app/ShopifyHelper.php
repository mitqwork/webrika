<?php

namespace App;

class ShopifyHelper
{

    private $shopifySDK;
    private $limitPageItemsForOrders = 5;
    private $limitPageItemsForProducts = 5;

    /**
     * Create a new ShopifyHelper instance.
     *
     * @return void
     */
    public function __construct()
    {

        $shopifyConfig = array(
            'ShopUrl' => config('shopify.ShopUrl'),
            'ApiKey' => config('shopify.ApiKey'),
            'Password' => config('shopify.Password'),
            'SharedSecret' => config('shopify.SharedSecret'),
        );

        $this->limitPageItemsForOrders = config('shopify.limitPageItemsForOrders');
        $this->limitPageItemsForProducts = config('shopify.limitPageItemsForProducts');

        $this->shopifySDK = new \PHPShopify\ShopifySDK($shopifyConfig);

    }

    public function getAllProducts()
    {
        return $this->shopifySDK->Product->get();
    }

    public function setShopifyProductData($id, $title, $body_html, $price)
    {
        
        $productData = [
            'title' => $title,
            'body_html' => $body_html,
        ];

        $this->shopifySDK->Product($id)->put($productData);

        $getVariantID = $this->shopifySDK->Product($id)->get()['variants'][0]['id'];

        $vatiantData = [
            'price' => $price
        ];

        $this->shopifySDK->Product($id)->Variant($getVariantID)->put($vatiantData);

        return $this->shopifySDK->Product($id)->get();
    }

    public function getAllOrders()
    {
        return $this->shopifySDK->Order->get();
    }

    public function getPaginationInfoForOrders()
    {
        $all = $this->shopifySDK->Order->count();
        $limit = $this->limitPageItemsForOrders;
        $lastPage = (int) (ceil($all/$limit));

        return [
            'all' => $all,
            'limit' => $limit,
            'lastPage' => $lastPage,
        ];

    }

    public function getPaginationInfoForProducs()
    {

        $all = $this->shopifySDK->Product->count();
        $limit = $this->limitPageItemsForOrders;
        $lastPage = (int) (ceil($all/$limit));

        return [
            'all' => $all,
            'limit' => $limit,
            'lastPage' => $lastPage,
        ];
    }

    public function getProductsByPagination($page = 1,  $limit = NULL)
    {

        if(is_null($limit)){
            $limit = $this->limitPageItemsForProducts;
        }

        if(!is_null($limit)){
            $this->limitPageItemsForProducts = $limit;
        }

        if((int) $page < 1){
            $page = 1;
        }

        return $this->shopifySDK->Product->get(['page' => (int) $page, 'limit' => (int) $limit]);
    }

    public function getOrdersByPagination($page = 1,  $limit = NULL)
    {


        if(is_null($limit)){
            $limit = $this->limitPageItemsForOrders;
        }

        if(!is_null($limit)){
            $this->limitPageItemsForOrders = $limit;
        }

        if((int) $page < 1){
            $page = 1;
        }


        
        return $this->shopifySDK->Order->get(['page' => (int) $page, 'limit' => (int) $limit]);
    }



}
