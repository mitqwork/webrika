<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopifyHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $ShopifyHelper = new ShopifyHelper();

        if((is_null($request->productPage)) || ((int) $request->productPage < 1))
        {
            $request->productPage = 1;
        }

        if((is_null($request->orderPage)) || ((int) $request->orderPage < 1))
        {
            $request->orderPage = 1;
        }

        $products = $ShopifyHelper->getProductsByPagination((int) $request->productPage);
        $paginationInfoForProducs = $ShopifyHelper->getPaginationInfoForProducs() + ['actualPage' => (int) $request->productPage];

        $orders = $ShopifyHelper->getOrdersByPagination((int) $request->orderPage); 
        $paginationInfoForOrders = $ShopifyHelper->getPaginationInfoForOrders() + ['actualPage' => (int) $request->orderPage];

        return view('home')
        ->with(compact('products', 'orders', 'paginationInfoForOrders', 'paginationInfoForProducs'));  
    }
    

    public function updateProductData($id, $title, $body_html, $price)
    {
        $ShopifyHelper = new ShopifyHelper();

        return $ShopifyHelper->setShopifyProductData($id, $title, $body_html, $price);
        
    }
}
